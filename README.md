# java-docker-practice-frontend 使用說明書

## 概述

會員系統、聊天系統

## 程式說明

### 會員系統

1. 註冊會員。填寫帳號、密碼、姓名、生日、自介、照片
2. 成功後取得 2FA QRCode
3. 登入網站
4. 用手機上 Authenticator 取得 OTP並輸入
5. 成功登入網站

### 聊天系統

1. 點選聊天功能
2. 可在輸入框發送消息

## 操作說明

1. 啟動IDE
2. 啟動 terminal
3. 先 npm install 來安裝需要的部件
4. npm run dev

## 注意事項

Node.js 版本

`v20.11.0`

npm 版本

`10.2.4`

如果專案沒有 .env.local，請在**src同層中創檔案**，內容是: **VITE_AXIOS_HTTP_BASEURL=http://localhost:8091/practice5**

## File Structure

```sh
─src
    │  App.vue
    │  main.js
    │
    ├─assets
    │      logo.svg
    │
    ├─components
    │  │  ActivityComponents.vue
    │  │  MemberComponents.vue
    │  │  Navbar.vue
    │  │  WelcomeItem.vue
    │  │
    │  └─icons
    │          IconCommunity.vue
    │          IconDocumentation.vue
    │          IconEcosystem.vue
    │          IconSupport.vue
    │          IconTooling.vue
    │
    ├─plugins
    │      vuetify.js
    │
    ├─router
    │      index.js
    │
    ├─stores
    │      counter.js
    │
    └─views
            AboutView.vue
            ActivityCenter.vue
            HomeView.vue
            Login.vue
            Logout.vue
            MemberCenter.vue
            MFA.vue
            Register.vue
```
