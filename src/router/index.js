import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/core/HomeView.vue'
import Login from '@/views/member/Login.vue'
import Register from '@/views/member/Register.vue'
import Logout from '@/views/member/Logout.vue'
import MemberCenter from '@/views/member/MemberCenter.vue'
import ActivityCenter from '@/views/activity/ActivityCenter.vue'
import MFA from '@/views/mfa/MFA.vue'
import Discussion from '@/views/activity/Discussion.vue'
import CreateActivityDetail from '@/views/activity/CreateActivityDetail.vue'
import ExportExcel from '@/views/document/ExportExcel.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/core/AboutView.vue')
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/logout',
      name: 'logout',
      component: Logout
    },
    {
      path: '/register',
      name: 'register',
      component: Register
    },
    {
      path: '/memberCenter',
      name: 'memberCenter',
      component: MemberCenter
    },
    {
      path: '/activityCenter',
      name: 'activityCenter',
      component: ActivityCenter
    },
    {
      path: '/mfa',
      name: 'mfa',
      component: MFA
    },
    {
      path: '/discussion/:activityDetailId',
      name: 'discussion',
      component: Discussion
    },
    {
      path: '/create/activity-detail',
      name: 'activityDetail',
      component: CreateActivityDetail
    },
    {
      path: '/exportExcel',
      name: 'exportExcel',
      component: ExportExcel
    },
  ]
})

export default router
