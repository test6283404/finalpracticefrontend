
import { createPinia } from 'pinia'
import { createApp } from 'vue'

import App from './App.vue'
import router from './router'
import axios from "axios"
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle.min.js";

// Vuetify
import '@mdi/font/css/materialdesignicons.css'
import 'vuetify/styles'
import { createVuetify } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'

const app = createApp(App)
const backendURL = import.meta.env.VITE_AXIOS_HTTP_BASEURL
app.config.globalProperties.$http = axios;

const httpClient = axios.create({
    withCredentials: true,
    baseURL: backendURL,
});

const vuetify = createVuetify({
    components,
    directives,
    ssr: true,
  })

app.use(createPinia())
app.use(router)
app.use(vuetify)
// app.use(bootstrap)

app.mount('#app')

export default httpClient;